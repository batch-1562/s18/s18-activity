
function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 1;
	this.attack = lvl;

	// methods
	this.tackle = function(target){
		while(target.health>10) {
		console.log(this.name + ' '+ 'tackled ' + target.name)
		target.health = target.health-this.attack;
		console.log(target.name + '\'s health is now reduced to '+target.health)
		
		} 
	};

	this.faint = function(){
		console.log(this.name + ' ' + 'fainted')
	};

}
// creating instance
let jiglypuff = new Pokemon('Jiglypuff', 10, 50);
let snorlax = new Pokemon('Snorlax', 10, 50);

jiglypuff.tackle(snorlax);
snorlax.tackle(jiglypuff);
jiglypuff.faint();